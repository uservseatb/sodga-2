
# Local Development

## first of all you have to install NPM and install all required npm packages
## to to that follow the instruction below

- google how to install NPM and install it
- go into sodga root folder 
- run `npm install`

## building css

`npm run sass`
`npm run build:css`

## building java script with source map

`npm run create:js`

## watching java script with source map

`npm run watch:js`


# deploy to server 

## run script

`./dpl.sh`

