import {Point} from "../dto/Point";
import Monitor from "../controls/Monitor";
import {LineCap, LineStyle} from "../dto/LineStyle";

const INCHES_MULTIPLIER: number = 25.4
const MM_MULTIPLIER: number = 1

export class GerberCommandProcessor {
    private monitor: Monitor
    private currentApertureAddress: number
    private aperturesMap: Map<number, Aperture> = new Map<number, Aperture>()
    private lastPosition: Point
    private poligonFirstPosition: Point
    private paintingMode: PaintingMode
    private isPoligon: boolean = false
    private polarity: string = 'D'

    private metricMultiplier: number = INCHES_MULTIPLIER
    private formatScaler: number

    private commonMultiplier: number = 1.7
    private offsetX: number = 0
    private offsetY: number = 800

    constructor(monitor: Monitor) {
        this.monitor = monitor
    }

    processCommand(command: string) {
        if (command.indexOf("G54") == 0) {
            command = command.substr(3)
        }
        if (command.indexOf('%') == 0) {
            this.processDirective(command)
        }
        if (command == 'G36*') {
            this.isPoligon = true
        }
        if (command == 'G37*') {
            let lineStyle = this.calculateLineStyle()
            this.monitor.drawLineTo(this.poligonFirstPosition, lineStyle)
            this.monitor.stroke()
            this.isPoligon = false
        }
        if (command == 'G01*') {
            this.paintingMode = PaintingMode.LINEAR
        }
        if (command == 'G02*') {
            this.paintingMode = PaintingMode.ARC
        }
        if (command == 'G03*') {
            this.paintingMode = PaintingMode.CS_ARC
        }
        if (command.indexOf('D') == 0) {
            this.currentApertureAddress = +command.substr(1, command.length - 2)
        }
        if (command.indexOf('X') == 0) {
            const scaler = this.formatScaler * 0.1 * (1 / this.commonMultiplier)
            // expected XxxxYxxx[D01|D02|D03]
            let args: string[] = command
                .replace("Y", " Y")
                .replace("D", " D")
                .split(" ")
                .filter(c => c.length > 0)
            let currentPosition = new Point(
                (+(args[0].substr(1)) / scaler) * this.metricMultiplier + this.offsetX,
                (+(args[1].substr(1)) / scaler) * this.metricMultiplier + this.offsetY
            )
            let action: Action = GerberCommandProcessor.detectAction(args[2]);
            if (action == Action.MOVE) {
                this.monitor.setPositionTo(currentPosition)
                if (this.isPoligon) {
                    this.poligonFirstPosition = currentPosition
                }
            }
            if (action == Action.PAINT) {
                let lineStyle = this.calculateLineStyle()
                if (!this.isPoligon) {
                    this.monitor.setPositionTo(this.lastPosition)
                }
                this.monitor.drawLineTo(currentPosition, lineStyle)
                if (!this.isPoligon) {
                    this.monitor.stroke()
                }
            }
            if (action == Action.FLASH) {
                let aperture = this.getCurrentAperture()
                aperture.draw(currentPosition, this.monitor)
            }
            this.lastPosition = currentPosition
        }
    }

    private processDirective(directive: string) {
        if (directive.indexOf("%AD") == 0) {
            let apertureMultiplier = 10 * this.metricMultiplier * this.commonMultiplier
            let ap = new Aperture(directive, apertureMultiplier)
            this.aperturesMap.set(ap.address, ap)
        }
        if (directive.indexOf("%LPD") == 0) {
            this.polarity = 'D'
        }
        if (directive.indexOf("%LPC") == 0) {
            this.polarity = 'C'
        }
        if (directive.indexOf("%MOMM") == 0) {
            this.metricMultiplier = MM_MULTIPLIER
        }
        if (directive.indexOf("%MOIN") == 0) {
            this.metricMultiplier = INCHES_MULTIPLIER
        }
        if (directive.indexOf("%FS") == 0) {
            // %FSLAX36Y36*%
            let scaleDigitsCount = directive.substr(7, 1)
            this.formatScaler = Math.pow(10, +scaleDigitsCount)
        }
    }

    private calculateLineStyle() {
        if (this.isPoligon) {
            return new LineStyle(0.3, LineCap.ROUND)
        }
        let currentAperture = this.getCurrentAperture()
        let lineWeight: number = currentAperture.args[0]
        return new LineStyle(lineWeight, LineCap.ROUND)
    }

    private getCurrentAperture() {
        if (!this.currentApertureAddress) {
            throw new DOMException("Illegal state: currentApertureAddress is undefined")
        }
        return this.aperturesMap.get(this.currentApertureAddress)
    }

    private static detectAction(actionCode: string) {
        if (actionCode == 'D01*') {
            return Action.PAINT
        }
        if (actionCode == 'D02*') {
            return Action.MOVE
        }
        if (actionCode == 'D03*') {
            return Action.FLASH
        }
        throw new DOMException("unknown action: [" + actionCode + "]")
    }
}

enum PaintingMode {
    LINEAR, ARC, CS_ARC
}

enum Action {
    PAINT, MOVE, FLASH
}

class Aperture {
    address: number
    type: ApertureType
    args: number[]

    constructor(addApertureCommand: string, apertureMultiplier: number) {
        // adding aperture
        const ADDRESS_FIRST_INDEX = 4 // %ADD23....
        let addressIndexLength: number = 1
        while (isCharNumber(addApertureCommand.charAt(ADDRESS_FIRST_INDEX + addressIndexLength))) {
            addressIndexLength++
        }
        this.address = +addApertureCommand.substr(ADDRESS_FIRST_INDEX, addressIndexLength)
        let apertureTypeCode = addApertureCommand.substr(ADDRESS_FIRST_INDEX + addressIndexLength, 1)
        this.type = typeByCode(apertureTypeCode)

        const APERTURE_TYPE_CODE_LENGTH = 2
        const ADD_APERTURE_COMMAND_END_LENGTH = 2
        this.args = addApertureCommand
            .substr(
                ADDRESS_FIRST_INDEX + addressIndexLength + APERTURE_TYPE_CODE_LENGTH,
                addApertureCommand.length - (
                    ADDRESS_FIRST_INDEX +
                    addressIndexLength +
                    APERTURE_TYPE_CODE_LENGTH +
                    ADD_APERTURE_COMMAND_END_LENGTH
                )
            )
            .split("X")
            .map(arg => +arg * apertureMultiplier)

        console.info("aperture created: [addr:" + this.address + ", type:" + this.type + ", args:" + this.args)

        function typeByCode(typeCode: string) {
            if (typeCode == 'C') return ApertureType.CIRCLE
            if (typeCode == 'R') return ApertureType.RECTANGLE
            throw new DOMException("unknown aperture typeCode='" + typeCode + "'")
        }

        function isCharNumber(c: string) {
            return c >= '0' && c <= '9';
        }
    }

    draw(position: Point, monitor: Monitor) {
        if (this.type == ApertureType.CIRCLE) this.__drawCircle(position, monitor)
        if (this.type == ApertureType.RECTANGLE) this.__drawRectangle(position, monitor)
    }

    private __drawCircle(position: Point, monitor: Monitor) {
        monitor.drawCircle(position, this.args[0])
    }

    private __drawRectangle(position: Point, monitor: Monitor) {
        monitor.drawRectangle(position, this.args[0], this.args[1])
    }
}

enum ApertureType {
    RECTANGLE, CIRCLE
}
