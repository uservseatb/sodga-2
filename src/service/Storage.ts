import {LocalStorage as localStorage} from "../../modules/LocalStorage";

export default class Storage {

    get(key: string): any {
        let backup: any = localStorage.getBackup() == null ? {} : localStorage.getBackup();
        return backup[key] ? JSON.parse(backup[key]) : {};
    }

    put(key: string, data: any): any {
        let backup: any = localStorage.getBackup() == null ? {} : localStorage.getBackup();
        backup[key] = JSON.stringify(data);
        localStorage.applyBackup(backup);
    }
}