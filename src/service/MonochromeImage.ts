export class MonochromeImage {
    private data: boolean[] = []
    private width: number
    private height: number

    private constructor() {
    }

    static ofImageData(imageData: ImageData): MonochromeImage {
        return new MonochromeImage().ofImageData(imageData)
    }

    extractContour(): MonochromeImage {
        let monochromeImage = new MonochromeImage();
        monochromeImage.width = this.width
        monochromeImage.height = this.height
        let previousValue = this.data[0]

        this.data.forEach((currentValue, index) => {
            if (currentValue && !previousValue) {
                monochromeImage.data[index] = true
            } else if (!currentValue && previousValue) {
                monochromeImage.data[index - 1] = true
            } else {
                monochromeImage.data[index] = false
            }
            previousValue = currentValue
        })

        previousValue = this.data[this.toVerticalCoordinates(0)]
        for (let i = 0; i < this.width * this.height; i++) {
            let index = this.toVerticalCoordinates(i)
            let currentValue = this.data[index]
            if (currentValue && !previousValue) {
                monochromeImage.data[index] = true
            } else if (!currentValue && previousValue) {
                monochromeImage.data[this.toVerticalCoordinates(i - 1)] = true
            }
            previousValue = currentValue
        }
        return monochromeImage

    }

    private toVerticalCoordinates(i: number): number {
        let col = Math.round(i / this.height)
        let row = i % this.height

        return row * this.width + col
    }

    cleanHalf(): MonochromeImage {
        let monochromeImage = new MonochromeImage()
        this.data.forEach((v, i) => {
            monochromeImage.data[i] = i > this.width * this.height / 2 ? v : false
        })
        monochromeImage.width = this.width
        monochromeImage.height = this.height
        return monochromeImage
    }

    get(index: number) {
        return this.data[index];
    }

    private ofImageData(imageData: ImageData) {
        let _data = imageData.data
        this.width = imageData.width
        this.height = imageData.height
        _data.forEach((v, i) => {
            if (i % 4 == 0) {
                this.data[i / 4] = _data[i] > 0 || _data[i + 1] > 0 || _data[i + 2] > 0 || _data[i + 3] > 0
            }
        })
        return this
    }
}