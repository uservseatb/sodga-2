import BaseElement from "./BaseElement";
import {Point} from "../dto/Point";
import {LineCap, LineStyle} from "../dto/LineStyle";
import {MonochromeImage} from "../service/MonochromeImage";

export default class Monitor extends BaseElement<HTMLCanvasElement> {

    private context: CanvasRenderingContext2D

    private width: number
    private height: number
    private color: string

    constructor(elementId: string, width: number, height: number) {
        super(elementId);

        this.context = this.element.getContext('2d');
        this.context.imageSmoothingEnabled = true

        const dpr = window.devicePixelRatio || 1;

        this.element.width = this.element.scrollWidth * dpr;
        this.element.height = this.element.scrollHeight * dpr;

        this.width = this.element.width;
        this.height = this.element.height;

        this.color = "#ffffff"
    }

    drawMonochromeImage(image: MonochromeImage, color: Color, drawMode: DrawMode) {
        let imageData: ImageData = this.context.getImageData(0, 0, this.width, this.height);
        let data = imageData.data
        data.forEach((value, index) => {
            if (index % 4 == 0) {
                if (image.get(index / 4)) {
                    if (drawMode == DrawMode.CLEAR) {
                        data[index] = color.getData().r
                        data[index + 1] = color.getData().g
                        data[index + 2] = color.getData().b
                        data[index + 3] = color.getData().a
                    }
                    if (drawMode == DrawMode.ON_TOP) {
                        data[index] = combine(data[index], color.getData().r)
                        data[index + 1] = combine(data[index + 1], color.getData().g)
                        data[index + 2] = combine(data[index + 2], color.getData().b)
                        data[index + 3] = combine(data[index + 3], color.getData().a)
                    }
                } else {
                    if (drawMode == DrawMode.CLEAR) {
                        data[index] = 0
                        data[index + 1] = 0
                        data[index + 2] = 0
                        data[index + 3] = 0
                    }
                }
            }
        })
        if (drawMode == DrawMode.CLEAR) {
            this.clear()
        }
        this.context.putImageData(imageData, 0, 0)

        function combine(current: number, newValue: number) {
            if (current == 0 && newValue > 0) {
                return newValue
            } else if (newValue == 0 && current > 0) {
                return current
            } else {
                return (current + newValue) / 2
            }
        }
    }

    clear() {
        this.context.clearRect(0, 0, this.width, this.height);
    }

    setPositionTo(point: Point) {
        this.context.beginPath();
        this.context.moveTo(point.x, this.__mirrorY(point.y));
    }

    drawLineTo(point: Point, lineStyle: LineStyle) {

        let lineCap: CanvasLineCap = ["butt", "round", "square"][lineStyle.lineCap.valueOf()] as CanvasLineCap

        this.context.strokeStyle = this.color
        this.context.lineWidth = lineStyle.width
        this.context.lineCap = lineCap
        this.context.lineTo(point.x, this.__mirrorY(point.y))
    }

    drawCircle(position: Point, diameter: number) {
        this.setPositionTo(position)
        this.drawLineTo(position, new LineStyle(diameter, LineCap.ROUND))
        this.stroke()
    }

    drawRectangle(position: Point, width: number, height: number) {
        this.context.beginPath()
        this.context.fillStyle = this.color
        this.context.fillRect(position.x - width / 2, this.__mirrorY(position.y + height / 2), width, height)
    }

    stroke() {
        this.context.stroke();
    }

    getWidth() {
        return this.width
    }

    getHeight() {
        return this.height
    }

    getMonochromeImage() {
        return MonochromeImage.ofImageData(
            this.context.getImageData(
                0, 0, this.width, this.height
            )
        )
    }

    private __mirrorY(y: number) {
        return this.height - y
    }
}

export class ColorData {
    r: number
    g: number
    b: number
    a: number

    constructor(r: number, g: number, b: number, a: number) {
        this.r = r
        this.g = g
        this.b = b
        this.a = a
    }
}

export class Color {
    private data: ColorData

    private constructor() {
    }

    static RED = Color.ofRgba(234, 1, 1, 255)
    static ORANGE = Color.ofRgba(234, 127, 1, 255)
    static GREEN = Color.ofRgba(11, 156, 49, 255)
    static SMOOTH = Color.ofRgba(150, 200, 150, 255)

    static ofString(color: string): Color {
        return new Color().ofString(color)
    }

    static ofRgba(r: number, g: number, b: number, a: number): Color {
        return new Color().ofRgba(r, g, b, a)
    }

    getData(): ColorData {
        return this.data
    }

    private ofRgba(r: number, g: number, b: number, a: number): Color {
        this.data = new ColorData(r, g, b, a)
        return this
    }

    private ofString(color: string): Color {

        return Color.RED
    }
}

export enum DrawMode {
    CLEAR, ON_TOP
}