import BaseElement from "./BaseElement";

export default class TextAreaInput extends BaseElement<HTMLTextAreaElement> {

    constructor(elementId: string) {
        super(elementId);
    }

    value(): string {
        return this.element.value;
    }
}