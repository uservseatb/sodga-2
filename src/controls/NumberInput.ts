import BaseElement from "./BaseElement";

export default class NumberInput extends BaseElement<HTMLInputElement> {

    constructor(elementId: string) {
        super(elementId);
    }

    value(): number {
        return +(this.element.value);
    }

    setValue(value: number): void {
        this.element.value = value == null ? "" : ("" + value);
    }

    onchange(listener: Function): NumberInput {
        this.element.onkeyup = <any>listener;
        return this;
    }
}