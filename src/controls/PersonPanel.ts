import NumberInput from "./NumberInput";
import GenderSelector from "./GenderSelector";
import Storage from "../service/Storage";
import Person from "../dto/Person";
import BaseElement from "./BaseElement";
import CommonWindow from "./CommonWindow";

const STORAGE_KEY = "personPanel.person";
const ELEMENT_ID = "body-params";
export default class PersonPanel extends BaseElement<HTMLDivElement> {

    storage: Storage;
    commonWindow: CommonWindow;
    bodyWeightInput: NumberInput;
    gender: GenderSelector;
    onchangeListener: Function;

    constructor() {
        super("body-params");
        this.storage = new Storage();
        this.commonWindow = new CommonWindow();
        this.bodyWeightInput = new NumberInput("body-weight");
        this.gender = new GenderSelector("gender-selector");
        let storedValue = this.storage.get(STORAGE_KEY);
        this.gender.setValue((<Person>storedValue).gender);
        this.bodyWeightInput.setValue((<Person>storedValue).bodyWeight);

        this.bodyWeightInput.onchange(() => {
            this.storage.put(STORAGE_KEY, this.value());
            this.onchangeListener(this.value())
        });

        this.gender.onchange(() => {
            this.storage.put(STORAGE_KEY, this.value());
            this.onchangeListener(this.value())
        });

        this.commonWindow.clickInsideOf(
            ELEMENT_ID, () => this.removeClass("collapsed")
        );

        this.commonWindow.clickOutOf(
            ELEMENT_ID, () => this.addClass("collapsed")
        );
    }

    value(): Person {
        return new Person(
            this.gender.value(),
            this.bodyWeightInput.value()
        );
    }

    onchange(listener: Function): PersonPanel {
        this.onchangeListener = listener;
        return this;
    }
}