export default class BaseElement<T extends HTMLElement> {

    element: T;

    constructor(elementId: string) {
        this.element = <T>document.getElementById(elementId);
    }

    addClass(className: string) {
        this.element.classList.add(className);
    }

    removeClass(className: string) {
        this.element.classList.remove(className);
    }
}