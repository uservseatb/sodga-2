import BaseElement from "./BaseElement";
import {CalculationResult} from "../dto/CalculationResult";


export default class ResultPanel extends BaseElement<HTMLDivElement> {

    constructor(elementId: string) {
        super(elementId);
        this.showInitialState();
    }

    showResult(info: CalculationResult) {


        var htmlResult = "<div class='promile-digit'>" + numeral(info.concentration).format('0.000') + "<span class='promile-text'> промилле</span> </div>";
        htmlResult += "<div class='promile-sign'></div>";
        htmlResult += "<div class='min'>минимальное время выведения: ";
        htmlResult += "<span class='hours'>" + numeral(info.minTime.hours).format('0') + " час " + numeral(info.minTime.minutes).format('0') + " мин</span></div>";
        htmlResult += "<div class='averade'>среднее время выведения: "
        htmlResult += "<span class='hours'>" + numeral(info.averageTime.hours).format('0') + " час " + numeral(info.averageTime.minutes).format('0') + " мин</span></div>";
        htmlResult += "<div class='max'>максимальное время выведения: "
        htmlResult += "<span class='hours'>" + numeral(info.maxTime.hours).format('0') + " час " + numeral(info.maxTime.minutes).format('0') + " мин</span></div>";
        this.element.innerHTML = htmlResult;
    }

    showInitialState() {
        this.element.innerHTML = "" +
            "<div class='info'>" +
            "Для расчёта концентрации алкоголя и приблизительного времени " +
            "полного выведения из крови добавте выпитые напитки" +
            "</div>";
    }

    clearResultPanel() {
        this.element.innerHTML = "";
    }
}