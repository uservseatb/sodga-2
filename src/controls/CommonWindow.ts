export default class CommonWindow {

    handlersForOutOf: any;
    handlersForInsideOf: any;

    constructor() {
        this.handlersForOutOf = {};
        this.handlersForInsideOf = {};
        window.onclick = (event: MouseEvent) => {
            for (let elementId in this.handlersForOutOf) {
                if (!this.handlersForOutOf.hasOwnProperty(elementId)) continue;
                if (CommonWindow.isEventOutOfElement(event, elementId)) {
                    this.handlersForOutOf[elementId]();
                }
            }
            for (let elementId in this.handlersForInsideOf) {
                if (!this.handlersForInsideOf.hasOwnProperty(elementId)) continue;
                if (!CommonWindow.isEventOutOfElement(event, elementId)) {
                    this.handlersForInsideOf[elementId]();
                }
            }
        }
    }

    clickOutOf(elementID: string, listener: Function): void {
        this.handlersForOutOf[elementID] = listener;
    }

    clickInsideOf(elementID: string, listener: Function): void {
        this.handlersForInsideOf[elementID] = listener;
    }

    private static isEventOutOfElement(event: MouseEvent, checkedElementId: string): boolean {

        return false
        // let parentElement = event.toElement;
        //
        // do {
        //     if (parentElement.id == checkedElementId) {
        //         return false;
        //     }
        //     parentElement = parentElement.parentElement;
        // }
        // while (parentElement !== null);
        //
        // return true;
    }
}