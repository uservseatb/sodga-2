
const FULL_STOMACH_RATIO = 0.7;
const EMPTY_STOMACH_RATIO = 0.9;

export default class StomachSelector {

    fullStomachChoice: HTMLInputElement;
    emptyStomachChoice: HTMLInputElement;

    constructor() {
        this.fullStomachChoice = <HTMLInputElement>document.getElementById("full-stomach-choice");
        this.emptyStomachChoice = <HTMLInputElement>document.getElementById("empty-stomach-choice");
    }

    ratio(): number {
        return this.fullStomachChoice.checked ? FULL_STOMACH_RATIO : EMPTY_STOMACH_RATIO;
    }
}