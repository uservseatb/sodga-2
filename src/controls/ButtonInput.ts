import BaseElement from "./BaseElement";

export default class ButtonInput extends BaseElement<HTMLInputElement> {

    constructor(elementId: string) {
        super(elementId);
    }

    onclick(listener: Function): ButtonInput {
        this.element.onclick = <any>listener;
        return this;
    }
}