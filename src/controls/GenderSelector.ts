import Gender from "../dto/Gender";

export default class GenderSelector {

    genderSelector: HTMLSelectElement;

    constructor(elementId: string) {
        this.genderSelector = <HTMLSelectElement>document.getElementById(elementId);
    }

    value(): Gender {
        return this.genderSelector.value == 'female' ? Gender.FEMALE : Gender.MALE;
    }

    setValue(gender: Gender): GenderSelector {
        if (gender === Gender.FEMALE) {
            this.genderSelector.value = 'female';
        } else {
            this.genderSelector.value = 'male';
        }
        return this;
    }

    onchange(listener: Function): GenderSelector {
        this.genderSelector.onchange = <any>listener;
        return this;
    }
}