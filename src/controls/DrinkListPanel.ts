import BaseElement from "./BaseElement";
import NumberInput from "./NumberInput";
import Drink from "../dto/Drink";
import ButtonInput from "./ButtonInput";

export default class DrinkListPanel extends BaseElement<HTMLDivElement> {

    drinks: Drink[] = [];

    drinkVolumeInput: NumberInput;
    drinkStrengthInput: NumberInput;
    addDrinkButton: ButtonInput;
    cleanDrinkButton: ButtonInput;

    listener: Function;

    constructor(elementId: string) {
        super(elementId);
        this.drinkVolumeInput = new NumberInput("drink-volume");
        this.drinkStrengthInput = new NumberInput("drink-strength");

        this.addDrinkButton = new ButtonInput("add-drink-button");
        this.addDrinkButton.onclick(() => this.addDrink());
        this.cleanDrinkButton = new ButtonInput("reset-button");
        this.cleanDrinkButton.onclick(() => this.resetDrinks());
    }

    getDrinks(): Drink [] {

        var drinkVolume: number = this.drinkVolumeInput.value();
        var drinkStrength: number = this.drinkStrengthInput.value();

        var result: Drink[] = [];

        for (var i = 0; i < this.drinks.length; i++) {
            result.push(this.drinks[i]);
        }

        if (drinkVolume > 0 && drinkStrength > 0) {
            result.push(new Drink(drinkVolume, drinkStrength));
        }
        return result;
    }

    setOnResetListener(listener: Function) {
        this.listener = listener;
        return this;
    }

    private createDrinkListText(drinks: Drink[]) {
        if (drinks.length === 0) {
            return "";
        }
        var text = "";
        for (var i = 0; i < drinks.length; i++) {
            text += "<div>";
            text += drinks[i].volume;
            text += " мл | ";
            text += drinks[i].strength;
            text += " %<br>";
            text += "</div>";
        }
        return text;
    }

    private addDrink() {
        gtag('event', 'AddDrink');
        var drinkVolume = this.drinkVolumeInput.value();
        var drinkStrength = this.drinkStrengthInput.value();

        if (drinkVolume > 0 && drinkStrength > 0) {

            this.clearInputs();

            this.drinks.push(new Drink(drinkVolume, drinkStrength));
            this.element.innerHTML = this.createDrinkListText(this.drinks);
        }
    }

    private resetDrinks() {
        gtag('event', 'Reset');

        this.clearInputs();

        this.element.innerHTML = "";
        this.drinks.splice(0, this.drinks.length)

        this.listener();
    }

    private clearInputs() {
        this.drinkVolumeInput.setValue(null);
        this.drinkStrengthInput.setValue(null);
    }
}