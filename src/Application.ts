import ButtonInput from "./controls/ButtonInput";
import TextAreaInput from "./controls/TextAreaInput";
import BaseElement from "./controls/BaseElement";
import Monitor, {Color, DrawMode} from "./controls/Monitor";
import {Point} from "./dto/Point";
import {GerberCommandProcessor} from "./service/GerberCommandProcessor";
import {MonochromeImage} from "./service/MonochromeImage";

export default class Application extends BaseElement<HTMLDivElement> {
    private sourceInput: TextAreaInput;
    private loadSourceButton: ButtonInput;
    private monitor: Monitor;

    constructor() {
        super("main-content");
        this.sourceInput = new TextAreaInput("text-source")
        this.loadSourceButton = new ButtonInput("load-source-button");
        this.loadSourceButton.onclick(() => this.loadSource());
        this.monitor = new Monitor("monitor", 1800, 1200);
        this.monitor.clear();
        this.removeClass("hidden")
    }

    private loadSource() {
        this.monitor.clear();
        let points: Point[] = []
        let gerberRaw = this.sourceInput.value()

        let gerberCommangs = gerberRaw
            .split("\n")
            .filter(filterGerberCommands)

        let gerberCommandProcessor: GerberCommandProcessor = new GerberCommandProcessor(this.monitor)

        gerberCommangs.forEach(command => {
            gerberCommandProcessor.processCommand(command)
        })

        let image: MonochromeImage = this.monitor.getMonochromeImage()
        this.monitor.drawMonochromeImage(image, Color.SMOOTH, DrawMode.CLEAR)
        this.monitor.drawMonochromeImage(image.extractContour(), Color.ORANGE, DrawMode.ON_TOP)

        function filterGerberCommands(line: string) {
            let trimmedLine = line.trim()
            return trimmedLine.length > 0
                && trimmedLine.indexOf("G04") != 0
        }
    }
}