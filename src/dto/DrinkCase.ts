import Gender from "./Gender";
import Drink from "./Drink";
import Person from "./Person";

export default class DrinkCase {

    person: Person;
    stomachRatio: number;
    drinkList: Drink [] = [];

    withPerson(person: Person) {
        this.person = person;
        return this;
    }

    withStomachRatio(stomachRatio: number) {
        this.stomachRatio = stomachRatio;
        return this;
    }

    withDrinkList(drinkList: Drink[]) {
        this.drinkList = drinkList;
        return this;
    }

    canBeCalculated() {
        return this.person.bodyWeight > 0 && this.drinkList.length > 0;
    }

}