import {Time} from "./Time";

export class CalculationResult {
    concentration: number;
    minTime: Time;
    averageTime: Time;
    maxTime: Time;

    constructor(concentration: number, minTime: Time, averageTime: Time, maxTime: Time) {
        this.concentration = concentration;
        this.minTime = minTime;
        this.averageTime = averageTime;
        this.maxTime = maxTime;
    }
}