export class LineStyle {
    width: number;
    lineCap: number;

    constructor(width: number, lineCap: LineCap) {
        this.width = width;
        this.lineCap = lineCap;
    }
}

export enum LineCap {
    BUTT, ROUND, SQUARE
}
