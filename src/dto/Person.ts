import Gender from "./Gender";

export default class Person {

    gender: Gender;
    bodyWeight: number;

    constructor(gender: Gender, bodyWeight: number) {
        this.gender = gender;
        this.bodyWeight = bodyWeight;
    }
}