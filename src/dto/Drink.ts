
export default class Drink {

    volume: number;
    strength: number;

    constructor(volume : number, strength : number) {
        this.volume = volume;
        this.strength = strength;
    }
}