const path = require('path');

const TerserPlugin = require("terser-webpack-plugin");

const NODE_ENV = process.env.NODE_ENV || 'development';

module.exports = {
    entry: './main.ts',
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            },
        ],
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js'],
    },
    output: {
        filename: 'index.js',
        path: path.resolve(__dirname),
    },
    optimization: {
        minimizer: NODE_ENV !== 'prod' ? [] : [new TerserPlugin()],
    },
    devtool: NODE_ENV !== 'prod' ? "inline-source-map" : false,
    mode: NODE_ENV !== 'prod' ? "production" : "development"
};